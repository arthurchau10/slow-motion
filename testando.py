from PIL import Image
import threading
from SlowmotionInference import SlowmotionInference

import numpy as np
import logging

slowmotionInference = SlowmotionInference('models/network-l1.pytorch', 0)
img1 = np.asarray(Image.open('images/first.png'))
img2 = np.asarray(Image.open('images/second.png'))

img15 = slowmotionInference.infer(img1, img2)
Image.fromarray(img15).save('teste.png', "PNG")
