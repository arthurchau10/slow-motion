import math
from PIL import Image
import threading
import torch
import torch.utils.serialization
from SlowmotionInference import SlowmotionInference

import numpy as np
import logging



logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',
                    )


class gpuThread(threading.Thread):
	def __init__(self, weights, device_id, upsample):
		threading.Thread.__init__(self)

		self.SlowmotionInference = SlowmotionInference(weights=weights, device_id=device_id)
		self.canStop = False

		self.frameBuffer = []
		self.frameFiles = []

		self.resultsBuffer = []
		self.numFramesPerOriginal = 0

		self.upsample = upsample
		self.frameCounter = 0
		self.sizeFrameBuffer = 0

		self.numFramesPerOriginal = self.upsample - 1
		self._lstResults = [0]*(self.numFramesPerOriginal + 2)


	def _recursiveInference(self, n):
		n_idx = 0
		for n_idx in range(int(self.upsample -n)):
			if not type(self._lstResults[int(n + n_idx + 1)]) is int:
				n_idx += 1
				break
			
		self._lstResults[int(n)] = self.SlowmotionInference.infer(self._lstResults[int(n - n_idx)], self._lstResults[int(n + n_idx)])

		n_left = (2*n - n_idx) * 0.5
		n_right = (2*n + n_idx) * 0.5
		if n_left.is_integer() == False or n_right.is_integer() == False:
			return
		self._recursiveInference(n_left)
		self._recursiveInference(n_right)
		return



	def run(self):
		self.sizeFrameBuffer = len(self.frameBuffer)
		while self.frameCounter < self.sizeFrameBuffer - 1:
			self._lstResults = [0]*(self.numFramesPerOriginal + 2)
			self._lstResults[0] = self.frameBuffer[self.frameCounter]
			self._lstResults[self.upsample] = self.frameBuffer[self.frameCounter + 1]

			self._recursiveInference(self.upsample/2)
			self.frameCounter += 1
			self.resultsBuffer = self.resultsBuffer + self._lstResults[1:self.upsample]

		self.SlowmotionInference = None

	    	#logging.debug('Ending thread')
