#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Autor: Edmundo Hoyle v1.0 Junho 2016
# Tecnologia P&D 
import os, sys
from subprocess import check_call
import subprocess


import getopt
from PIL import Image

import numpy as np
import skimage, time, datetime, math, re

import logging
import threading
from multiprocessing import Process

#scripts desenvolvidos
from gpuThread import gpuThread

from time import sleep
import sys


weights = 'models/network-lf.pytorch'


##STRINGS FFMPEG
STR_FFMPEG_EXTRACT = ("ffmpeg -nostats -loglevel 0 -i %s -vf yadif=0:0:0  -qscale:v 2 %s")

## Variaveis Globais
str_path_video      = ""
str_path_frames     = ""
str_path_slow      = ""
str_path_video_mask = ""
str_ffmpeg_CLI      = STR_FFMPEG_EXTRACT
lst_label_type      = [192,128,128]
int_gpu_count       = 1
int_tipo_video        = 0
int_upsample         = 2
float_fps            = 30


'''
def copyLOG2S3(name_video):
    try:
        str_comando = "sed '/convolution/d' /home/ubuntu/tmp/erro.log | " + "awk sub'" + '("$", "\r")' + "' > /home/ubuntu/s3/logs/" + name_video + ".txt"
        #str_comando = "sed '/convolution/d' /home/ubuntu/tmp/erro.log | tr -d '\15\32' > /home/ubuntu/s3/logs/" + name_video + ".txt" 
        #str_comando = "cp /home/ubuntu/tmp/erro.log /home/ubuntu/s3/logs/" + name_video + ".txt"
        subprocess.call(str_comando, shell=True)
    except:
        print("Nao foi possivel copiar o log para o S3")
'''


def itsOk(str_args):
    global str_path_video, str_path_frames, str_path_slow, str_path_video_mask, lst_label_type, LST_LABELS, int_gpu_count, int_upsample, float_fps
    try:
        lst_optlist, lst_bad_args = getopt.getopt(str_args, 'hi:f:m:o:v:n:x:')
    except:
        print ("Argumentos desconhecidos")
        print ("-h para ver as opcoes")
        exit(0)
    if lst_bad_args:
        print ("Argumentos desconhecidos: %s" % str(lst_bad_args))
        print ("-h para ver as opcoes")
        exit(0)
    if len(lst_optlist) < 5:
        print("Precisa de 5 argumentos no minimo. Argumentos possiveis: -i <video> (opcional) -f <pasta_frames> -m <pasta_frames_slowmotion> ...")
        print("... -o <video_slowmotion> (opcional) -v <framerate> -n <numero de gpus> -x <fator de upsample>")
        exit(0)
    for str_tpl_opts in lst_optlist:
        #### Verifica se os argumentos estão corretos

        if str_tpl_opts[0] == '-n':
            try:
                int_gpu_count = int(str_tpl_opts[1])
            except:
                print("Numero de gpus desconhecido.")
                exit(0)

        if str_tpl_opts[0] == '-x':
            try:
                int_upsample = int(str_tpl_opts[1])
            except:
                print("Fator de upsample invalido.")
                exit(0)  

        if str_tpl_opts[0] == '-v':
            try:
                float_fps = float(str_tpl_opts[1])
            except:
                print("Framerate invalido.")
                exit(0)  

        # Verifica a pasta dos frames
        if str_tpl_opts[0] == '-f':
            str_path_frames = str_tpl_opts[1]
            if not(os.path.isdir(str_path_frames)):
                os.mkdir(str_path_frames)

        
        # Verifica a pasta das mascaras
        if str_tpl_opts[0] == '-m': 
            str_path_slow = str_tpl_opts[1]
            if not(os.path.isdir(str_path_slow)):
                os.mkdir(str_path_slow)
            else:
                filelist = os.listdir(str_path_slow)
                for f in filelist:
                    os.remove(os.path.join(str_path_slow, f))

        # Pega a localização do video
        if str_tpl_opts[0] == '-i': 
            str_path_video = str_tpl_opts[1] 
            if not(os.path.exists(str_path_video)):
                print("O video <%s> nao existe" % str_path_video)
                exit(0)
            if os.path.isdir(str_path_video):
                print("Path de video invalido. (diretorio detectado)" % str_path_video)
                exit(0)


        # info do video que vai ser criado
        if str_tpl_opts[0] == '-o': 
            str_path_video_mask = str_tpl_opts[1]



    #Listando Procedimento
    print ("")
    print ("S L O W - M O T I O N")
    print ("- - - - - - - - - - - - - - - - - -")
    print("Config List")
    print("Video Original   : %s" % str_path_video)
    print("Frames em        : %s" % str_path_frames)
    print("SlowMotion Frames em : %s" % str_path_slow)
    print ("Fator de upsampling: %s" % int_upsample)
    print ("Num GPUs: %d" % int_gpu_count)
    print("Video Final       : %s" % str_path_video_mask)

    print ("- - - - - - - - - - - - - - - - - -")
    print ("")

def extract_frames_ffmpeg(path_video, path_frames):
    global str_ffmpeg_CLI
    int_resp = -1
    try:
        str_ffmpeg_CLI = (str_ffmpeg_CLI) % (path_video, os.path.join(path_frames, "frames_%5d.jpg"))
        print(str_ffmpeg_CLI)
        int_resp = check_call(str_ffmpeg_CLI.split(), shell=False)
    except IOError as e:
        print ("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print ("Could not convert data to an integer.")
    except:
        print ("Unexpected error:", sys.exc_info()[0])
    return int_resp == 0 

def create_video(str_path_video, str_path_slow, fps = 30000.0/1001.0):
    global str_ffmpeg_CLI
    int_resp = -1
    try:
        str_ffmpeg_CLI = ("ffmpeg -y -framerate " + str(fps) + " -i %s" % os.path.join(str_path_slow, "frames_%05d.png")  + " -vcodec libx264 -b:v 10000k -tune ssim -pix_fmt yuv420p " + str_path_video)
        print(str_ffmpeg_CLI)
        int_resp = check_call(str_ffmpeg_CLI.split(), shell=False)
    except IOError as e:
        print ("I/O error({0}): {1}".format(e.errno, e.strerror))
    except ValueError:
        print ("Could not convert data to an integer.")
    except:
        print ("Unexpected error:", sys.exc_info()[0])
    return int_resp == 0

def listFilesPath(path=None,extension=None):
    """
    Version 1.0 16/11/2016
    """
    if path is None:
        path = "."
    if extension is None:
        extension = ["*"] 
    if not(type(extension) is list):
        extension = [extension]
    files = []
    for a in os.listdir(path):
        for ext in extension:
            if re.search("^.*\." + ext + "$", a, re.IGNORECASE):
                files.append(a)
                continue
    return files 

logging.getLogger("requests").setLevel(logging.CRITICAL)
logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-10s) %(message)s',
                    )


def write_image(file_path, img):
    #cv2.imwrite(file_path, img)
    img.save(file_path, "PNG")

def main(args):
    global str_path_video, str_path_frames, str_path_slow, str_path_video_mask, float_fps, int_gpu_count
    itsOk(args) 
    #Definição de variáveis 
    int_meta = 10

    ############# PARTE 1 Extração do Video

    tempo_full_inicial = time.time()

    if str_path_video != '':
        filelist = os.listdir(str_path_frames)
        for f in filelist:
            os.remove(os.path.join(str_path_frames, f))

        print("Extraindo os frames do video.....")
        if not(extract_frames_ffmpeg(str_path_video, str_path_frames)):
            exit(0)
        print("Frames extraidos.")

    lst_frames_names = listFilesPath(str_path_frames, ['png', 'bmp', 'jpg', 'jpeg'])
    int_qt_frames = len(lst_frames_names)
    print("Criando slowmotion de %s frames ..." % str(int_qt_frames))
    
    ##################### PARTE 2 Processamento

    proc_threads = []
    for _gpu in range(int_gpu_count):
        newGpuThread = gpuThread(weights, device_id = _gpu, upsample=int_upsample)
        proc_threads.append(newGpuThread)
    
    ## Inicio do loop
    bol_ultimo_frame = False
    counter = 0

    lst_frames_names = sorted([f for f in lst_frames_names])
    frames_per_gpu = len(lst_frames_names)/int_gpu_count
    _gpu_idx = 0

    for _i, str_frame_name in enumerate(lst_frames_names):
        if _i%frames_per_gpu == 0 and _i/int_gpu_count < frames_per_gpu and _i != 0:
            _gpu_idx += 1
            img_frame = np.asarray(Image.open(os.path.join(str_path_frames, lst_frames_names[_i-1])))
            proc_threads[_gpu_idx].frameBuffer.append(img_frame)
            proc_threads[_gpu_idx].frameFiles.append(os.path.join(str_path_slow, lst_frames_names[_i-1].split(".")[0])) 

        img_frame = np.asarray(Image.open(os.path.join(str_path_frames, lst_frames_names[_i])))
        proc_threads[_gpu_idx].frameBuffer.append(img_frame)
        proc_threads[_gpu_idx].frameFiles.append(os.path.join(str_path_slow, lst_frames_names[_i].split(".")[0])) 


    print ("Inicializando " + str(len(proc_threads))+ " thread(s) . . .")
    #copyLOG2S3(os.path.basename(str_path_video))

    for _thread in proc_threads:
        _thread.start()

    progressPercent = 0.0
    num_frames_processados = 0
    tempo_inicial = time.time()
    print ("Threads inicializados . . .")


    num_samples_per_frame = int_upsample - 1
    #total_frames_result = (len(lst_frames_names) - 1)*num_samples_per_frame
    total_frames_result = 0
    for _thread in proc_threads:
        total_frames_result += len(_thread.frameBuffer)

    while True:
        time.sleep(int_meta)
        num_frames_processados = 0
        for _i, _thread in enumerate(proc_threads):
            num_frames_processados += _thread.frameCounter + 1

    
        num_frames_faltando = total_frames_result - num_frames_processados
        progressPercent = (num_frames_processados)*1./(total_frames_result)*100

        if num_frames_faltando <= 0:
            break

        if num_frames_processados > 0:
            print ('Progresso: ' + str(num_frames_processados) + ' frames de ' + str(total_frames_result) + ' ('+ "%.2f" %  progressPercent + '%)')
            tempo_estimado = (time.time() - tempo_inicial)*(1./num_frames_processados)*num_frames_faltando
            print ("Tempo estimado: " + time.strftime('%H:%M:%S', time.gmtime(tempo_estimado)))
            print ("Num frames faltando: " + str(num_frames_faltando))

        #copyLOG2S3(os.path.basename(str_path_video))


            
    
    print ("Finalizado a criacao dos slowmotions. Tempo corrido: " + time.strftime('%H:%M:%S', time.gmtime(time.time() - tempo_full_inicial)))
    print ("Salvando em disco . . .")
    #copyLOG2S3(os.path.basename(str_path_video))f
    frame_cnt = 0 

    for _thread in proc_threads:
        total_frames_result += len(_thread.resultsBuffer)

    total_frames = total_frames_result - 1*(int_gpu_count-1)



    for _i, _thread in enumerate(proc_threads):     
        for _j, fr in enumerate(_thread.frameBuffer):
            out_path = os.path.join(str_path_slow, ('frames_%05d' % frame_cnt) + '.png')
            if  _j != 0 or frame_cnt == 0:
                Image.fromarray(fr).save(out_path, "PNG")
                frame_cnt += 1
            for _k in range(num_samples_per_frame):
                if len(_thread.resultsBuffer) > 0:
                    out_path = os.path.join(str_path_slow, ('frames_%05d' % (frame_cnt)) +'.png')
                    Image.fromarray(_thread.resultsBuffer.pop(0)).save(out_path, "PNG")  
                    frame_cnt += 1


    '''
    for _i, _thread in enumerate(proc_threads):     
        for _j, fr in enumerate(_thread.frameBuffer):
            out_path = os.path.join(str_path_slow, ('frames_%05d' % frame_cnt) + '.png')
            if  _j != 0 or frame_cnt == 0:
                Image.fromarray(fr).save(out_path, "PNG")
                frame_cnt += 1
            if frame_cnt == total_frames:
                break
            for _k in range(num_samples_per_frame):
                if _j*num_samples_per_frame + _k >= len(_thread.resultsBuffer):
                    continue
                out_path = os.path.join(str_path_slow, ('frames_%05d' % (frame_cnt + _k)) +'.png')
                Image.fromarray(_thread.resultsBuffer[_j*num_samples_per_frame + _k]).save(out_path, "PNG")
            frame_cnt += num_samples_per_frame
    '''




    for _thread in proc_threads:
        _thread.join()

    print("Iniciando a criacao do video para download...")
    tempo_final = time.time()

    ################## PARTE 3 Criando o video 
    if str_path_video_mask != '':
        if not(create_video(str_path_video_mask, str_path_slow, float_fps)):
            exit(0)
    msg = "O processo de criacao do foi finalizado com sucesso.\n"
    msg += "Tempo utilizado: " + time.strftime('%H:%M:%S', time.gmtime(time.time() - tempo_full_inicial)) + "\n\n"
 
    msg += "Resultado em: %s\n" % str_path_video_mask

    print (msg)
    print("Video pronto para download")
    #copyLOG2S3(os.path.basename(str_path_video))

    
if __name__=="__main__":
    main (sys.argv[1:])
    
